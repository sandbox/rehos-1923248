<?php
/**
 * @file
* Features hooks for the field_localization features component.
*/

/**
 * Implements hook_features_export_options().
*/
function field_localization_features_export_options() {
  $options = array();

  $textgroup = i18n_string_textgroup('field');

  $language_list = locale_language_list('native', TRUE);

  foreach($language_list as $langcode => $language) {
    $instances = field_info_instances();
    foreach ($instances as $entity_type => $bundles) {
      foreach ($bundles as $bundle => $fields) {
        foreach ($fields as $field) {
          $strings = $textgroup->multiple_translation_search(array($field['field_name'], array('#field', '#allowed_values', $bundle), '*'), $langcode);
          
          if (count($strings)) {
            $identifier = "{$entity_type}-{$bundle}-{$field['field_name']}";
            $options[$langcode.':'.$identifier] = $identifier.' ('.$langcode.')';
          }
        }
      }
    }
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function field_localization_features_export($data, &$export, $module_name = '') {
  $pipe = array();
  
  $export['dependencies']['i18n_string'] = 'i18n_string';
  $export['dependencies']['i18n_field'] = 'i18n_field';
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['features_i18n'] = 'features_i18n';
  
  foreach ($data as $localization) {
    list($langcode, $identifier) = explode(':', $localization);
    
    $pipe['field'][] = $identifier;
    
    $pipe['language'][] = $langcode;
     
    $export['features']['field_localization'][$localization] = $localization;
  }

  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function field_localization_features_export_render($module = 'foo', $data) {
  $translatables = $code = array();

  $textgroup = i18n_string_textgroup('field');

  $code[] = '  $localizations = array();';
  $code[] = '';

  foreach ($data as $localization) {
    list($langcode, $identifier) = explode(':', $localization);
    list($entity_type, $bundle, $field_name) = explode('-', $identifier);
      
//    $strings = $textgroup->multiple_translation_search(array($field_name, array('#field', '#allowed_values', $bundle), '*'), $langcode);

    $strings = $textgroup->multiple_translation_search(array($field_name, $bundle, '*'), $langcode);    
    $properties = array();
    foreach($strings as $string) {
      $properties[$string->property] = $string->get_translation($langcode);
    }
    ksort($properties);
    
    $strings = $textgroup->multiple_translation_search(array($field_name, '#allowed_values', '*'), $langcode);
    $allowed_values = array();
    foreach($strings as $string) {
      $allowed_values[$string->property] = $string->get_translation($langcode);
    }    
    ksort($allowed_values);
   
    if(count($properties) || count($allowed_values)) {
      $export = array(
          'entity_type' => $entity_type,
          'bundle' => $bundle,
          'field_name' => $field_name,
          'langcode' => $langcode,
      );
      
      if (count($properties)) {
        $export['properties'] = $properties;
      }

      if (count($allowed_values)) {
        $export['allowed_values'] = $allowed_values;
      }

      $code[] = '  $localizations[] = ' . features_var_export($export, '  ') . ';';
    }
  }

  if (!empty($translatables)) {
    $code[] = features_translatables_export($translatables, '  ');
  }

  $code[] = '  return $localizations;';
  $code = implode("\n", $code);
  return array('field_localization_default_localizations' => $code);
}

/**
 * Implements hook_features_revert().
 */
function field_localization_features_revert($module) {
  field_localization_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function field_localization_features_rebuild($module) {
  $localizations = module_invoke($module, 'field_localization_default_localizations');
  if (!empty($localizations)) {
    $textgroup = i18n_string_textgroup('field');
    foreach ($localizations as $data) {
      $bundle = $data['bundle'];
      $field_name = $data['field_name'];
      $langcode = $data['langcode'];
      if (isset($data['properties'])) {
        foreach($data['properties'] as $property => $value) {
          $textgroup->update_translation(array($field_name, $bundle, $property), $langcode, $value);
        }
      }
      if (isset($data['allowed_values'])) {
        foreach($data['allowed_values'] as $property => $value) {
          $textgroup->update_translation(array($field_name, '#allowed_values', $property), $langcode, $value);
        }
      }
    }
  }
}
