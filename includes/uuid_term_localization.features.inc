<?php
/**
 * @file
 * Features hooks for the uuid_term_localization features component.
 */

/**
 * Implements hook_features_export_options().
 */
function uuid_term_localization_features_export_options() {
  $options = array();

  $textgroup = i18n_string_textgroup('taxonomy');

  $language_list = locale_language_list('native', TRUE);

  foreach($language_list as $langcode => $language) {
    $strings = $textgroup->multiple_translation_search(array('term', '*', '*'), $langcode);

    foreach($strings as $string) {
      $term = taxonomy_term_load((int)$string->objectid);
      if ($term !== FALSE && i18n_taxonomy_vocabulary_mode($term->vid, I18N_MODE_LOCALIZE)) {
        $taxonomy = taxonomy_vocabulary_load($term->vid);
        if ($taxonomy !== FALSE) {
          // Automatically export all localized properties for a term
          $options[$langcode.':'.$term->uuid] = $taxonomy->name.' - '.$term->name.' ('.$langcode.')';
      }
    }
  }
}

return $options;
}

/**
 * Implements hook_features_export().
 */
function uuid_term_localization_features_export($data, &$export, $module_name = '') {
  $pipe = array();
  
  $export['dependencies']['taxonomy'] = 'taxonomy';
  $export['dependencies']['uuid'] = 'uuid';
  $export['dependencies']['i18n_string'] = 'i18n_string';
  $export['dependencies']['i18n_taxonomy'] = 'i18n_taxonomy';
  $export['dependencies']['uuid_features'] = 'uuid_features';
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['features_i18n'] = 'features_i18n';
  
  $language_list = array();

  foreach ($data as $localization) {
    list($langcode, $uuid) = explode(':', $localization);
     
    $terms = entity_uuid_load('taxonomy_term', array($uuid));
    if (count($terms)) {
      $term = reset($terms);
      
      $pipe['language'][] = $langcode;
      $pipe['uuid_term'][] = $uuid;
       
      uuid_term_localization_features_get_dependencies($export, $term, $langcode);
    }
  }

  language_features_export($language_list, $export, $module_name);

  return $pipe;
}

/**
 * Adds term localizations and its dependencies to the export.
 *
 * Parents and term references are handled recursively
 *
 */
function uuid_term_localization_features_get_dependencies(&$export, $term, $langcode) {
  $textgroup = i18n_string_textgroup('taxonomy');

  $strings = $textgroup->multiple_translation_search(array('term', array($term->tid), '*'), $langcode);
   
  if (count($strings)) {
    $export['features']['uuid_term_localization'][$langcode.':'.$term->uuid] = $langcode.':'.$term->uuid;
  }

  // Recursively add all/any parent localizations
  foreach (taxonomy_get_parents($term->tid) as $parent) {
    if (!in_array($langcode.':'.$parent->uuid, $export['features']['uuid_term_localization'])) {
      uuid_term_localization_features_get_dependencies($export, $parent, $langcode);
    }
  }
}

/**
 * Implements hook_features_export_render().
 */
function uuid_term_localization_features_export_render($module = 'foo', $data) {
  $translatables = $code = array();

  $textgroup = i18n_string_textgroup('taxonomy');

  $code[] = '  $localizations = array();';
  $code[] = '';
  foreach ($data as $localization) {
    list($langcode, $uuid) = explode(':', $localization);
     
    // @todo reset = TRUE as otherwise references (parent, fields) were destroyed.
    $terms = entity_uuid_load('taxonomy_term', array($uuid), array(), TRUE);
    if (!count($terms)) {
      continue;
    }

    $term = reset($terms);

    $strings = $textgroup->multiple_translation_search(array('term', array($term->tid), '*'), $langcode);

    $properties = array();

    foreach($strings as $string) {
      $properties[$string->property] = $string->get_translation($langcode);
    }
    
    ksort($properties);

    $export = array(
        'uuid' => $term->uuid,
        'langcode' => $langcode,
        'properties' => $properties,
    );

    $code[] = '  $localizations[] = ' . features_var_export($export, '  ') . ';';
  }

  if (!empty($translatables)) {
    $code[] = features_translatables_export($translatables, '  ');
  }

  $code[] = '  return $localizations;';
  $code = implode("\n", $code);
  return array('uuid_term_localization_default_localizations' => $code);
}

/**
 * Implements hook_features_revert().
 */
function uuid_term_localization_features_revert($module) {
  uuid_term_localization_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function uuid_term_localization_features_rebuild($module) {
  $localizations = module_invoke($module, 'uuid_term_localization_default_localizations');
  if (!empty($localizations)) {
    $textgroup = i18n_string_textgroup('taxonomy');
    foreach ($localizations as $data) {
      $ids = entity_get_id_by_uuid('taxonomy_term', array($data['uuid']));
      if (!empty($ids)) {
        $id = $ids[$data['uuid']];
        $langcode = $data['langcode'];
        foreach($data['properties'] as $property => $value) {
          $textgroup->update_translation(array('term', $id, $property), $langcode, $value);
        }
      }
    }
  }
}
